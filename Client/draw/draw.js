function setup() {
  createCanvas(displayWidth, displayHeight);
  strokeWeight(10);
  stroke(color(12,122,12));
}

function touchMoved() {
  line(mouseX, mouseY, pmouseX, pmouseY);
  change(mouseX,mouseY,pmouseX,pmouseY)
  return false;
}



var add = "localhost"
var client = new WebSocket('ws://' +add+':3345/', 'game-protocol');

client.onopen = function(connection) {


    client.onmessage = function (event) {
        console.log("Received: '" + event.data + "'");
        data = JSON.parse(event.data);
        line(data.mx,data.my,data.px,data.py)

    }
}

function change(mx,my,px,py) {
    client.send(JSON.stringify({"mx":mx,"my":my,"px":px,"py":py}));
}

class Rectangle  extends Obj {
	constructor(posX, posY,length, width=length,color=rot){
        super(posX, posY, length/2 + width/2);
		this.length = length;
		this.width = width;
		this.color = color;
	}

	intersects(other){
        if(other instanceof Line){
			return ((this.x > other.x && this.x < other.x + other.width ) ||
				   (this.x +this.width > other.x && this.x+this.width < other.x + other.width ))&&
				   ((this.y > other.y && this.y < other.y + other.length ) ||
				   (this.y +this.length > other.y && this.y +this.length < other.y + other.length));
		} else if(other instanceof Rectangle){
			return ((this.x > other.x && this.x < other.x + other.width ) ||
				   (this.x +this.width > other.x && this.x+this.width < other.x + other.width ))&&
				   ((this.y > other.y && this.y < other.y + other.length ) ||
				   (this.y +this.length > other.y && this.y +this.length < other.y + other.length));

		}else if(other instanceof Circle){
    		return dist(this.x,this.y,other.x,other.y) < other.radius/2 ||
				   dist(this.x+this.width,this.y,other.x,other.y) < other.radius/2 ||
				   dist(this.x,this.y+this.length,other.x,other.y) < other.radius/2 ||
				   dist(this.x+this.width,this.y+this.length,other.x,other.y) < other.radius/2;
        }else{
    		return dist(this.x,this.y,other.x,other.y) < other.radius/2 + this.radius/2;
        }
	}

	draw(){
        this.fixWall();
		rectShad(this.x-relX,this.y-relY,this.width,this.length,0,2,this.color);
	}

	fixWall(){
		if(this.x + this.width >= windowWidth){
		  this.x = windowWidth - this.width;
	  }if(this.x - this.width <= 1 ){
		  this.x = this.width;
		}if(this.y + this.length >= windowHeight){
		  this.y = windowHeight - this.length;
		}if(this.y - this.length <=  1){
		  this.y = this.length;
		}
	}

	checkWall(){
		if(this.x + this.width >= gameWidth){
			return true;
		}if(this.x - this.width <= 1 ){
			return true;
		}if(this.y + this.length >= gameHeight){
			return true;
		}if(this.y - this.length <=  1){
			return true;
		}
		return false;
	}
}

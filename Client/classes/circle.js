class Circle  extends Obj{

	constructor(posX, posY, radius,color=color(0)){
        super(posX, posY, radius);
        this.color = color;
    }


	intersects(other){
	        if(other instanceof Line){
    		return dist(other.x,other.y,this.x,this.y) < this.radius ||
				   dist(other.x+other.width,other.y+other.length,this.x,this.y) < this.radius ||
				   dist(other.x,other.y+other.length,this.x,this.y) < this.radius ||
				   dist(other.x+other.width,other.y+other.length,this.x,this.y) < this.radius;
			}else if(other instanceof Rectangle){
    		return dist(other.x,other.y,this.x,this.y) < this.radius ||
				   dist(other.x+other.width,other.y+other.length,this.x,this.y) < this.radius ||
				   dist(other.x,other.y+other.length,this.x,this.y) < this.radius ||
				   dist(other.x+other.width,other.y+other.length,this.x,this.y) < this.radius;
        }else if(other instanceof Circle){
    		return dist(this.x,this.y,other.x,other.y) < other.radius/2 + this.radius/2;
        }else{
    		return dist(this.x,this.y,other.x,other.y) < other.radius/2 + this.radius/2;
        }
	}

	draw(){
        this.fixWall();
		rectShad(this.x-relX,this.y-relY,this.radius,this.radius,this.radius,2,this.color);
	}


        fixWall(){
            if(this.x + this.radius/2 >= gameWidth){
              this.x = gameWidth - this.radius/2;
          }if(this.x - this.radius/2 <= 0 ){
              this.x = this.radius/2;
          }if(this.y + this.radius/2 >= gameHeight){
              this.y = gameHeight - this.radius/2;
          }if(this.y - this.radius/2 <= 0){
              this.y = this.radius/2;
            }
        }

}

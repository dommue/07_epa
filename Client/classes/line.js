class Line  extends Obj {
	constructor(posX, posY,length, width=length,size=1,color=rot){
        super(posX, posY, length/2 + width/2);
		this.size = size;
		this.length = length;
		this.width = width;
		this.color = color;
	}

	intersects(other){
        if(other instanceof Line){
			return ((this.x > other.x && this.x < other.x + other.width ) ||
				   (this.x +this.width > other.x && this.x+this.width < other.x + other.width ))&&
				   ((this.y > other.y && this.y < other.y + other.length ) ||
				   (this.y +this.length > other.y && this.y +this.length < other.y + other.length));

		}else if(other instanceof Rectangle){
			return ((this.x > other.x && this.x < other.x + other.width ) ||
				   (this.x +this.width > other.x && this.x+this.width < other.x + other.width ))&&
				   ((this.y > other.y && this.y < other.y + other.length ) ||
				   (this.y +this.length > other.y && this.y +this.length < other.y + other.length));

		}else if(other instanceof Circle){
    		return dist(this.x,this.y,other.x,other.y) < (other.radius/2)+(this.size/3) ||
				   dist(this.x+this.width,this.y,other.x,other.y) < (other.radius/2)+(this.size/3)||
				   dist(this.x,this.y+this.length,other.x,other.y) < (other.radius/2)+(this.size/3)||
				   dist(this.x+this.width,this.y+this.length,other.x,other.y) <  (other.radius/2)+(this.size/3);
        }else{
    		return dist(this.x,this.y,other.x,other.y) < other.radius/2 + this.radius/2;
        }
	}

	draw(){
		line(this.x-relX,this.y-relY,this.x - relX + this.width, this.y - relY + this.length);
		stroke(0)
		strokeWeight(1);
	}

	fixWall(){
		if(this.x + this.width >= windowWidth){
		  this.x = windowWidth - this.width;
	  }if(this.x - this.width <= 1 ){
		  this.x = this.width;
		}if(this.y + this.length >= windowHeight){
		  this.y = windowHeight - this.length;
		}if(this.y - this.length <=  1){
		  this.y = this.length;
		}
	}

	checkWall(){
		if(this.x + this.width >= gameWidth){
			return true;
		}if(this.x - this.width <= 1 ){
			return true;
		}if(this.y + this.length >= gameHeight){
			return true;
		}if(this.y - this.length <=  1){
			return true;
		}
		return false;
	}
}

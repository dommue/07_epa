	class Bullet  extends Line {
		constructor(posX, posY,length, width,xVel,yVel,damage = 1,size = 4,ttl = 0,mine=false,color){
	        super(posX, posY,length, width,size,color);
			this.xVel = xVel;
			this.yVel = yVel;
			this.mine = mine;
			this.damage = damage;
			this.ttl = ttl;
		}
		draw(){
			this.ttl -= 1;
			if(this.ttl == 0){
				this.delete();
			}
	        this.x += this.xVel;
	        this.y += this.yVel;
			if(this.mine){
				this.kill();
			}
	        this.fixWall();
			strokeWeight(this.size);
			stroke(this.color)

			line(this.x-relX, this.y-relY, this.x-relX + this.width, this.y-relY + this.length);
			stroke(0)
			strokeWeight(0.1);
		}

		kill(){
			var ded = this.intersectsAny();
			for(let d of ded){
				if(d instanceof Player){
					if(d.name != me.name){
						damage(d.name,this.damage);
					}
				}
			}
		}

		fixWall(){
			let intersects = false
			if(this.mine){
				for(let d of this.intersectsAny()){
					if(d instanceof Player){
						intersects = true
					}
				}
			}
			if(this.checkWall()||intersects){
				this.delete()
	        }
		}

		delete(){
	        for( var i = allObjects.length; i--;){
	            if ( allObjects[i] === this){
					allObjects.splice(i, 1);
					return;
	            }
	        }
		}

	}

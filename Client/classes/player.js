class Player  extends Circle{
    constructor(posX, posY, radius,name,color=color(0),live=2,xVel=0,yVel=0){
        super(posX, posY, radius,color);
        this.name = name;
        this.live = live;
        this.xVel = xVel;
        this.yVel = yVel;
    }

    	draw(){
            this.fixWall();
    		rectShad(this.x-relX,this.y-relY,this.radius,this.radius,this.radius,2,this.color);
            rectShad(this.x-relX+(this.radius*this.live/1000)/2 -this.radius/2,this.y-relY+(this.radius/2)+14,this.radius*this.live/1000,10,0,2,color(26, 242, 98))
            fill(0)
            text(this.name,this.x-relX-(this.radius/4),this.y-relY)
	        this.x += this.xVel;
	        this.y += this.yVel;
    	}
}

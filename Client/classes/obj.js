class Obj  {
	constructor(posX, posY,radius){
		this.x = posX;
		this.y = posY;
        this.radius = radius;
	}

	draw(){
	}

	intersects(other){
        if(other instanceof Obj){
    		return dist(this.x,this.y,other.x,other.y) < other.radius/2 + this.radius/2;
        }else{
             return other.intersects(this);
        }
	}

	intersectsAny(){
		let over = [];
		for(let other of allObjects){
			if(this !== other && this.intersects(other)){
				over.push(other);
			}
		}
		return over;
	}


	checkWall(){
		if(this.x + this.radius >= windowWidth){
            return true;
	    }if(this.x - this.radius <= 1 ){
            return true;
		}if(this.y + this.radius >= windowHeight){
            return true;
	    }if(this.y - this.radius <=  1){
            return true;
		}
        return false;
	}
}

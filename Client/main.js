
//keycodeList copied from  https://keycode.info
var keycodeList = [{id: 0, name: "That key has no keycode"},{id: 3, name: "break"},{id: 8, name: "backspace / delete"},{id: 9, name: "tab"},{id: 12, name: "clear"},{id: 13, name: "enter"},{id: 16, name: "shift"},{id: 17, name: "ctrl"},{id: 18, name: "alt"},{id: 19, name: "pause/break"},{id: 20, name: "caps lock"},{id: 21, name: "hangul"},{id: 25, name: "hanja"},{id: 27, name: "escape"},{id: 28, name: "conversion"},{id: 29, name: "non-conversion"},{id: 32, name: "spacebar"},{id: 33, name: "page up"},{id: 34, name: "page down"},{id: 35, name: "end"},{id: 36, name: "home"},{id: 37, name: "left arrow"},{id: 38, name: "up arrow"},{id: 39, name: "right arrow"},{id: 40, name: "down arrow"},{id: 41, name: "select"},{id: 42, name: "print"},{id: 43, name: "execute"},{id: 44, name: "Print Screen"},{id: 45, name: "insert"},{id: 46, name: "delete"},{id: 47, name: "help"},{id: 48, name: "0"},{id: 49, name: "1"},{id: 50, name: "2"},{id: 51, name: "3"},{id: 52, name: "4"},{id: 53, name: "5"},{id: 54, name: "6"},{id: 55, name: "7"},{id: 56, name: "8"},{id: 57, name: "9"},{id: 58, name: ":"},{id: 59, name: "semicolon (firefox), equals"},{id: 60, name: "<"},{id: 61, name: "equals (firefox)"},{id: 63, name: "ß"},{id: 64, name: "@ (firefox)"},{id: 65, name: "a"},{id: 66, name: "b"},{id: 67, name: "c"},{id: 68, name: "d"},{id: 69, name: "e"},{id: 70, name: "f"},{id: 71, name: "g"},{id: 72, name: "h"},{id: 73, name: "i"},{id: 74, name: "j"},{id: 75, name: "k"},{id: 76, name: "l"},{id: 77, name: "m"},{id: 78, name: "n"},{id: 79, name: "o"},{id: 80, name: "p"},{id: 81, name: "q"},{id: 82, name: "r"},{id: 83, name: "s"},{id: 84, name: "t"},{id: 85, name: "u"},{id: 86, name: "v"},{id: 87, name: "w"},{id: 88, name: "x"},{id: 89, name: "y"},{id: 90, name: "z"},{id: 91, name: "Windows Key / Left ⌘ / Chromebook Search key"},{id: 92, name: "right window key"},{id: 93, name: "Windows Menu / Right ⌘"},{id: 95, name: "sleep"},{id: 96, name: "numpad 0"},{id: 97, name: "numpad 1"},{id: 98, name: "numpad 2"},{id: 99, name: "numpad 3"},{id: 100, name: "numpad 4"},{id: 101, name: "numpad 5"},{id: 102, name: "numpad 6"},{id: 103, name: "numpad 7"},{id: 104, name: "numpad 8"},{id: 105, name: "numpad 9"},{id: 106, name: "multiply"},{id: 107, name: "add"},{id: 108, name: "numpad period (firefox)"},{id: 109, name: "subtract"},{id: 110, name: "decimal point"},{id: 111, name: "divide"},{id: 112, name: "f1"},{id: 113, name: "f2"},{id: 114, name: "f3"},{id: 115, name: "f4"},{id: 116, name: "f5"},{id: 117, name: "f6"},{id: 118, name: "f7"},{id: 119, name: "f8"},{id: 120, name: "f9"},{id: 121, name: "f10"},{id: 122, name: "f11"},{id: 123, name: "f12"},{id: 124, name: "f13"},{id: 125, name: "f14"},{id: 126, name: "f15"},{id: 127, name: "f16"},{id: 128, name: "f17"},{id: 129, name: "f18"},{id: 130, name: "f19"},{id: 131, name: "f20"},{id: 132, name: "f21"},{id: 133, name: "f22"},{id: 134, name: "f23"},{id: 135, name: "f24"},{id: 144, name: "num lock"},{id: 145, name: "scroll lock"},{id: 160, name: "^"},{id: 161, name: "!"},{id: 162, name: "؛ (arabic semicolon)"},{id: 163, name: "#"},{id: 164, name: "$"},{id: 165, name: "ù"},{id: 166, name: "page backward"},{id: 167, name: "page forward"},{id: 168, name: "refresh"},{id: 169, name: "closing paren (AZERTY)"},{id: 170, name: "*"},{id: 171, name: "~ + * key"},{id: 172, name: "home key"},{id: 173, name: "minus (firefox), mute/unmute"},{id: 174, name: "decrease volume level"},{id: 175, name: "increase volume level"},{id: 176, name: "next"},{id: 177, name: "previous"},{id: 178, name: "stop"},{id: 179, name: "play/pause"},{id: 180, name: "e-mail"},{id: 181, name: "mute/unmute (firefox)"},{id: 182, name: "decrease volume level (firefox)"},{id: 183, name: "increase volume level (firefox)"},{id: 186, name: "semi-colon / ñ"},{id: 187, name: "equal sign"},{id: 188, name: "comma"},{id: 189, name: "dash"},{id: 190, name: "period"},{id: 191, name: "forward slash / ç"},{id: 192, name: "grave accent / ñ / æ / ö"},{id: 193, name: "?, / or °"},{id: 194, name: "numpad period (chrome)"},{id: 219, name: "open bracket"},{id: 220, name: "back slash"},{id: 221, name: "close bracket / å"},{id: 222, name: "single quote / ø / ä"},{id: 223, name: "`"},{id: 224, name: "left or right ⌘ key (firefox)"},{id: 225, name: "altgr"},{id: 226, name: "< /git >, left back slash"},{id: 230, name: "GNOME Compose Key"},{id: 231, name: "ç"},{id: 233, name: "XF86Forward"},{id: 234, name: "XF86Back"},{id: 235, name: "non-conversion"},{id: 240, name: "alphanumeric"},{id: 242, name: "hiragana/katakana"},{id: 243, name: "half-width/full-width"},{id: 244, name: "kanji"},{id: 251, name: "unlock trackpad (Chrome/Edge)"},{id: 255, name: "toggle touchpad"}]


var room = getQueryVariable("room");
var type = 0;
var gameWidth = 2000;
var gameHeight = 2000;
var velX = 0;
var velY = 0;
var add = window.location.host
var relX;
var relY;
var counter;
var rot;
var blau;
var me;
var name ="";
var data = {"players" : [], "shots": []}
var allObjects = []
var topP = []
var lives = 0;
var dirXBefore = 0;
var dirYBefore = 0;
var client;
var connection = false;
//keyboardlayout
var btns = [87,65,83,68,38,37,40,39]

function setup() {
        createCanvas(windowWidth, windowHeight);
        blau = color(255, 204, 0);
        rot = color(240, 0, 16);
        randX = Math.floor(Math.random()*(gameWidth-100))+50
        randY = Math.floor(Math.random()*(gameHeight-100))+50
        relX = randX - windowWidth/2;
        relY = randY - windowHeight/2;
        me = new Circle(randX,randY,100,blau)
        allObjects.push(me);
        counter = 0;
        textSize(32);

}

function draw() {
    if(lives != 0)
        heal(1)

    //make white background
    background(255);



    //grid
    stroke(240);
    strokeWeight(5);
    dis = 100;
    lineXmin = relX*-1
    if(lineXmin < 0)
    lineXmin = dis*-1 - (relX % dis)
    lineYmin = relY*-1
    if(lineYmin < 0)
    lineYmin = dis*-1 - (relY % dis)
    lineXmax = gameWidth - relX
    lineYmax = gameHeight - relY;

    for(let x = lineXmin; x <= lineXmax;x+=dis){
        line(x,lineYmin,x,lineYmax)
    }
    for(let y = lineYmin; y <= lineYmax;y+=dis){
        line(lineXmin,y,lineXmax,y)
    }
    strokeWeight(1);


    //Counter
    if(counter < 1000){
        counter += 1;
    }

    if (document.hasFocus()) {
        var gamepads = navigator.getGamepads();
        if(!! gamepads[0]){
            if(counter == 20){
                if(gamepads[0].buttons[0].pressed){//links
                }
                else if(gamepads[0].buttons[1].pressed){//unten
                }
                else if(gamepads[0].buttons[2].pressed){//oben
                }
                else if(gamepads[0].buttons[3].pressed){//rechts
                }
            }

            me.x -= 5*gamepads[0].axes[3];

             change()
         }
    }
    xAxis = 0;
    yAxis = 0;
    if (keyIsDown(btns[1]))
        xAxis = -1;
    if (keyIsDown(btns[3]))
        xAxis = 1;
    if (keyIsDown(btns[0]))
        yAxis = -1;
    if (keyIsDown(btns[2]))
        yAxis = 1;
    if (keyIsDown(btns[4]))
        shoot(0,-1)
    if (keyIsDown(btns[5]))
        shoot(-1,0)
    if (keyIsDown(btns[6]))
        shoot(0,1)
    if (keyIsDown(btns[7]))
        shoot(1,0)

    go(xAxis,yAxis);

    for(let o of allObjects){
        o.draw();
    }



    fill(0)
    //name
    text(name,windowWidth/2,4.5*windowHeight/6)
    //livebar
    rectShad(windowWidth/2,5*windowHeight/6,windowWidth*3/4,windowHeight/15,10,0,color(190))
    rectShad(windowWidth*1/8 + windowWidth*3*lives/8000,5*windowHeight/6,windowWidth*3*lives/4000,windowHeight/15,10,2,color(26, 242, 98))
    //leaderboard
    let xp = 1

    data.players.sort(function(a, b) {
      return b.points - a.points;
    });

    textSize(28);
    fill(color(40,40,40,120))
    for(let person of data.players.slice(0,5)){
        text(xp +". "+ person.name + " - " + person.points, windowWidth-300,xp*40)
        xp += 1
    }

    textSize(32);

    if(lives <= 0){
		fill(0,0,0,150);
		rect(0,0,windowWidth,windowHeight);
        fill(200,20,1)
        text("you dieded!", (windowWidth-120)/2,windowHeight/4)
        text("Press Space to Retry", (windowWidth-250)/2,windowHeight/3)
        if(keyIsDown(32)){
            ressurectPlayer()
            me.color = color(255, 204, 0);
            randX = Math.floor(Math.random()*(gameWidth-100))+50
            randY = Math.floor(Math.random()*(gameHeight-100))+50
            relX = randX - windowWidth/2;
            relY = randY - windowHeight/2;
            me.x = randX;
            me.y = randY;
        }
    }
}

function settings(){//display settings menue
    if(document.getElementById("controlls").style.visibility == "visible"){
        document.getElementById("controlls").style.visibility = "hidden";
    }else{
        document.getElementById("controlls").style.visibility = "visible";
    }
}

function getKey(key){
    for(let button of keycodeList){
        if(button.id == key)
            return button.name
    }
    return ""
}

function changeKey(t){

    document.getElementById("clickNow").style.visibility = "visible";
    document.querySelector('body').onkeydown = function(e) {//eat next input
        if (!e.metaKey) {
            e.preventDefault();
        }
        console.log(e.keyCode);
        console.log(getKey[e.keyCode])
        btns[t] = e.keyCode;
        document.getElementById("clickNow").style.visibility = "hidden";
        document.querySelector('body').onkeydown = function(e) {}//reset to normal afterwards :)
    }
}

function getKeys(){
    console.log( "btnGoUp :" + btns[0]  +getKey(btns[0]))
    console.log("btnGoLeft :"+ btns[1]  +getKey(btns[1]))
    console.log("btnGoDown :"+ btns[2]  +getKey(btns[2]))
    console.log("btnGoRight :"+btns[3]  +getKey(btns[3]))
    console.log("btnShootUp :"+btns[4]  +getKey(btns[4]))
    console.log("btnShootLeft :"+btns[5]  +getKey(btns[5]))
    console.log("btnShootDown :"+btns[6]  +getKey(btns[6]))
    console.log("btnShootRight :"+btns[7]  +getKey(btns[7]))
}

//function from https://stackoverflow.com/questions/28387719/how-can-i-grab-variable-from-url-using-javascript-and-display-it-on-screen
function getQueryVariable(variable)
{
       var vars = window.location.search.substring(1).split("&");
       for (var i=0;i<vars.length;i++) {
               var pair = vars[i].split("=");
               if(pair[0] == variable){return pair[1];}
       }return 1
}



//value between -1 - 1
function go(dirX, dirY){
    let velPlayer = 5;
    if(type == 2){
        velPlayer = 4;
    }
    if(type == 3){
        velPlayer = 8;
    }
    velX = dirX*velPlayer
    velY = dirY*velPlayer
    me.x += dirX*velPlayer;
    if(dirX < 0 && relX > windowWidth*-1/4 && me.x -relX < windowWidth/3){
        relX += dirX * velPlayer;
    }else if(relX < gameWidth - 3*windowWidth/4  && me.x - relX > 2*windowWidth/3){
        relX += dirX* velPlayer;
    }

    me.y += dirY*velPlayer;
    if(dirY < 0 && relY > windowHeight*-1/4 && me.y -relY < windowHeight/3){
        relY += dirY * velPlayer;
    }else if(relY < gameHeight - 3*windowHeight/4  && me.y - relY > 2*windowHeight/3){
        relY += dirY* velPlayer;
    }
    if(dirX != dirXBefore ||dirY != dirYBefore || dirX != 0 ||dirY != 0)
        change()
    dirYBefore = dirY;
    dirXBefore = dirX;
}


function shoot(dirX, dirY) {
    if(type == 0){
        if(counter >= 15){
            counter = 0;
            createBullet(dirX,dirY,45)
        }
    }else if(type == 1){
        if(counter >= 40){
            counter = 0;
            createBullet(dirX,dirY,20,15,100)
            let xy = rotateDeg(dirX,dirY,10)
            createBullet(xy[0],xy[1],20,15,100)
            xy = rotateDeg(dirX,dirY,-10)
            createBullet(xy[0],xy[1],20,15,100)
            xy = rotateDeg(dirX,dirY,20)
            createBullet(xy[0],xy[1],20,15,100)
            xy = rotateDeg(dirX,dirY,-20)
            createBullet(xy[0],xy[1],20,15,100)
        }
    }else if(type == 2){
        if(counter >= 100){
            counter = 0;
            createBullet(dirX,dirY,200,7,700,70)
        }
    }
    else if(type == 3){
        if(counter >= 10){
            counter = 0;
            for(let i = 0; i <= 360;i += 20){
                let xy = rotateDeg(0,1,i);
                createBullet(xy[0],xy[1],8,7,30);
            }
        }
    }
}

function rectShad(x,y,xDis,yDis,curve,depth,color){
    xDis =  Math.abs(xDis);
    yDis =  Math.abs(yDis);
    depth =  Math.abs(depth);
    curve =  Math.abs(curve);
	noStroke();
	var xDraw = xDis + depth * 5;
	var yDraw = yDis + depth * 5;
	var xyDraw = (xDraw + yDraw)/2;
	while(xDraw > xDis || yDraw > yDis){
		fill(0,0,0,map((xDraw+yDraw)/2,xyDraw,(xDis+yDis)/2,0,(15	-depth)/2));
		rect(x-(xDis/2),y-(yDis/2)+(2*depth),xDraw,yDraw,curve+(depth * 5));

		xDraw -= 1;
		yDraw -= 1;
	}
	fill(color);
	rect(x-(xDis/2),y-(yDis/2),xDis,yDis,curve);

}


function startConnection(){

    name = document.getElementById("name").value
    console.log(name)
    console.log(list)
    var list;

    const Http = new XMLHttpRequest();
    Http.open("GET", "https://"+  add +":3344/getInfo/" +room);
    Http.send();
    Http.onreadystatechange=(e)=>{
        if(Http.readyState == 4 && Http.status == 200){
            list = JSON.parse(Http.responseText).list;
            console.log(list)

            if(!! document.querySelector('input[name="class"]:checked')){
                id = document.querySelector('input[name="class"]:checked').id
                type = id[id.length - 1];
            }
            var element = document.getElementById("inputs");

            element.parentNode.removeChild(element);
            while(list.includes(name)){
                name = prompt("Nicht ok",localStorage.getItem('name'));
            }
            localStorage.setItem('name',name);
            client = new WebSocket('wss://' + add+':3344/', 'game-protocol');

            client.onopen = function(connection) {
                document.getElementById("settings").style.visibility = "visible";
                createPlayer()

                client.onmessage = function (event) {
                    data = JSON.parse(event.data);
                    if(data.info == "death"){
                        me.color = color(180,180,180);
                        lives = 0;
                    }else{
                        for(let d of data.shots){
                            if(d.name != name){
                                let b = new Bullet(d.posX, d.posY,d.length, d.width,d.xVel,d.yVel,d.damage,d.size,d.ttl,false,rot)
                                allObjects.push(b);
                            }
                        }

                        for(var i = allObjects.length; i--;){
                            if ( allObjects[i] instanceof Player){
                                allObjects.splice(i, 1);
                            }
                        }
                        for(let d of data.players){
                            if(d.name != name){
                                allObjects.push(new Player(d.x, d.y, 100,d.name,color(200,30,0),d.lives,d.xVel,d.yVel)) ;
                            }else{
                                lives = d.lives;
                            }
                        }
                    }
                }
            }
            connection = true;
            if (list == "undefined"){
                window.location.href = ".";
            }
        }
    }
}



function rotateDeg(x, y, deg) {
    return rotateRad(x, y, deg * (Math.PI/180))

}

function rotateRad(x, y, rad){
    var ca = Math.cos(rad);
    var sa = Math.sin(rad);
    return [ca*x - sa*y, sa*x + ca*y];
}


function createBullet(dirX,dirY,ttl = 0,speed = 20,dmg = 200,size = 4){
    if(connection){
        let b = new Bullet(me.x, me.y,dirY*speed,dirX*speed,dirX*speed,dirY*speed,dmg,size,ttl,true,rot);
        allObjects.push(b)
        bullet = {"function":"AddShot","room":room,"from":name,"data":{"size":b.size,"name":name,"posX":b.x, "posY":b.y,"length":b.length, "width":b.width,"xVel":b.xVel,"yVel":b.yVel,"damage":b.damage,"ttl":b.ttl}}
        client.send(JSON.stringify(bullet));
    }
}

function damage(other,dmg){
    if(connection)
        client.send(JSON.stringify({"function":"killPlayer","room":room,"from":name,"data":{"name" : other,"damage":dmg}}));
}


function heal(heal){
    if(connection)
        client.send(JSON.stringify({"function":"healPlayer","room":room,"from":name,"data":{"name" : name,"heal":heal}}));
}

function createPlayer() {
    if(connection)
        client.send(JSON.stringify({"function":"addPlayer","room":room,"from":name,"data":{"name" : name, "x" : 1000, "y" : 1000,"xVel": 0,"yVel": 0}}));
}

function change() {
    if(connection)
        client.send(JSON.stringify({"function":"changePlayer","room":room,"from":name,"data":{"name" : name, "x" : me.x, "y" : me.y,"xVel":velX,"yVel": velY}}));
}

function ressurectPlayer(){
    if(connection)
        client.send(JSON.stringify({"function":"resurrectPlayer","room":room,"from":name,"data":{"name" : name, "x" : 1000, "y" : 1000,"xVel": 0,"yVel": 0}}));
}



//main
const Http = new XMLHttpRequest();
Http.open("GET", "https://"+  add +":3344/getInfo/" +room);
Http.send();
Http.onreadystatechange=(e)=>{
    if(Http.readyState == 4 && Http.status == 200){
        if (JSON.parse(Http.responseText).list == "undefined"){
            window.location.href = ".";
        }
    }
}

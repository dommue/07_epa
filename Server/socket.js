#!/usr/bin/env node
var WebSocketServer = require('websocket').server;
const https = require('https');
const fs = require('fs');

const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};



//all variables:
var rooms = [];
var playerInfo = [];

function idFromRoom(room){
    for(i = 0; i<rooms.length;i++){
        if(rooms[i].id == room){
            return i
        }
    }
}

function addRoom(open = true){
    genId = Math.random().toString(36).replace('0.', '')
    console.log("created room: " + genId)
    rooms.push({"id":genId,"players":[],"shots":[],"open":open});
    return genId
}

function removeEmptyRooms(){
    for(i = 0; i<rooms.length;i++){
        if(rooms[i].players.length == 0){
            console.log("room deleted: "+rooms[i].id)
            rooms.splice(i,1);
        }
    }
}

function checkPlayer(con, js) { //make sure he exist & that his claims are valid (eg. address / room)
    for(let p of playerInfo){
        if(con.socket._peername.address + con.socket._peername.port ==
        p.connection.socket._peername.address + p.connection.socket._peername.port &&
        p.name == js.from && p.room == js.room){
            for(let pl of rooms[js.room].players){
                if(pl.name == p.name)
                    return true;
            }
        }
    }
    return false;
}

function resurrectPlayer(con,js){
    ok = false;
    for(let p of playerInfo){
        if(con.socket._peername.address == p.connection.socket._peername.address &&
             con.socket._peername.port == p.connection.socket._peername.port &&
        p.name == js.from && p.room == js.room){
            ok = true
        }
    }
    for(let pl of rooms[js.room].players){
        if(pl.name == js.from)
            ok = false;
    }
    if(ok){
        js.data.points = 0;
        js.data.lives = 1000;
        rooms[js.room].players.push(js.data);
        SendEveryone();
    }

}

function addPlayer(con, js){
    var ok = true;
    for(let p of playerInfo){
        if(p.name == js.from && p.room == js.room){
            ok = false;
            break;
        }
    }
    if (ok) {
        js.data.points = 0;
        playerInfo.push({"name" : js.from, "connection" : con, "room": js.room});
        js.data.lives = 1000;
        rooms[js.room].players.push(js.data);
        SendEveryone();
    }
}

function changePlayer(con, js) {
    if(checkPlayer(con,js)){
        for(let p of rooms[js.room].players){
            if(p.name == js.from){
                xbefore = p.x;
                ybefore = p.y;
                xVelBefore = p.xVel;
                yVelBefore = p.yVel;
                p.x = js.data.x;
                p.y = js.data.y;
                p.xVel = js.data.xVel;
                p.yVel = js.data.yVel;
                if (xbefore != js.data.x || ybefore != js.data.y
                    || xVelBefore != js.data.xVel || yVelBefore != js.data.yVel) {
                    SendEveryone();
                }
                return;
            }
        }
    }
}

function healPlayer(con,js) {
    if(checkPlayer(con,js)){
        for( var i = rooms[js.room].players.length; i--;){
            if(rooms[js.room].players[i].name == js.data.name){
                if(rooms[js.room].players[i].lives < 1000){
                    rooms[js.room].players[i].lives += js.data.heal;
                    if(rooms[js.room].players[i].lives > 1000){
                        rooms[js.room].players[i].lives = 1000;
                    }
                    SendEveryone();
                }
            }
        }
    }

}

function killPlayer(con, js) {
    if(checkPlayer(con,js)){
        for( var i = rooms[js.room].players.length; i--;){
            if(rooms[js.room].players[i].name == js.data.name){
                rooms[js.room].players[i].lives -= js.data.damage;
                SendEveryone();
                if(rooms[js.room].players[i].lives <= 0){
                    rooms[js.room].players.splice(i, 1);
                    for(let p of playerInfo){
                        if(p.name == js.data.name && p.room == js.room){
                            p.connection.sendUTF("{ \"info\": \"death\"}");
                            SendEveryone();
                        }
                    }
                }
            }else if(rooms[js.room].players[i].name == js.from){
                rooms[js.room].players[i].points += js.data.damage;

            }
        }
    }
}

function addShot(con, js) {
    if(checkPlayer(con,js)){
        rooms[js.room].shots.push(js.data);
        SendEveryone();
        rooms[js.room].shots = [];
    }
}

function removePlayer(con) {
    for( var i = playerInfo.length; i--;){
        if(con.socket._peername.address + con.socket._peername.port ==
        playerInfo[i].connection.socket._peername.address + playerInfo[i].connection.socket._peername.port){
            if(rooms.length > playerInfo[i].room){
                for( var j = rooms[playerInfo[i].room].players.length; j--;){
                    if(rooms[playerInfo[i].room].players[j].name == playerInfo[i].name){
                        rooms[playerInfo[i].room].players.splice(j, 1);
                        SendEveryone();
                        break;
                    }
                }
            }
            playerInfo.splice(i, 1);
            return;
        }
    }
}

//Every Request will be going thru here:
function RequestLogic(connection,data){
    js = JSON.parse(data);
    js.room = idFromRoom(js.room)
    if(! (js.room === undefined)){
        if(js.function == "addPlayer"){
            addPlayer(connection,js);
        }else if(js.function == "resurrectPlayer"){
            resurrectPlayer(connection,js);
        }else if(js.function == "killPlayer"){
            killPlayer(connection,js);
        }else if(js.function == "healPlayer"){
            healPlayer(connection,js);
        }else if(js.function == "changePlayer"){
            changePlayer(connection,js);
        }else  if(js.function == "getInfo"){
            sendInfo(connection);
        }else  if(js.function == "AddShot"){
            addShot(connection,js);
        }else{
            console.log("nicht ok");
        }
    }
}

function SendEveryone(){
    for(let p of playerInfo){
        sendInfo(p.connection,p);
    }
}

function sendInfo(con, js) {
    if(!! rooms[js.room])
        con.sendUTF(JSON.stringify(rooms[js.room]));
}

function getRooms() {
    let list = []
    for (r of rooms){
        if(r.open && r.players.length < 16){
            list.push(r.id)
        }
    }
    if(list.length == 0){
        list.push(addRoom());
    }
    return JSON.stringify({"list":list})
}

function getInfo(room){
    i = idFromRoom(room);
    if(i  === undefined){
        return JSON.stringify({"list":"undefined"})
    }
    var list = []
    for(let p of rooms[i].players){
        list.push(p.name)
    }
    return JSON.stringify({"list" : list });
}

function originIsAllowed(origin) {
    return true;
    let allowed = ["http://flyin.party"]
    return allowed.includes(origin);
}


var server = https.createServer(options, function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    if(request.url.startsWith("/getInfo/")){
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.writeHead(200);
        response.end(getInfo(request.url.split("/")[2]));
    }else if(request.url == "/getInfo"){
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.writeHead(200);
        let r = getRooms();
        response.end(r);
    }else if(request.url == "/getRoom"){
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.writeHead(200);
        let r = addRoom(false);
        response.end(r);
    }else if(request.url == "/"){
        response.writeHead(200);
        response.end("dont");
    }else{
        response.writeHead(404);
        response.end();
    }
});


server.listen(3344, function() {
    console.log((new Date()) + ' Server is listening on port 3344');
});


wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }

    var connection = request.accept('game-protocol', request.origin);
    console.log((new Date()) + ' Connection accepted.');

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            RequestLogic(connection,message.utf8Data)
        }
    });

    connection.on('close', function(reasonCode, description) {
        removePlayer(connection);
        removeEmptyRooms();
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});

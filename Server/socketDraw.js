#!/usr/bin/env node
var WebSocketServer = require('websocket').server;
var http = require('http');


//all variables:
var playerInfo = [];

function addPlayer(con){
    playerInfo.push(con);
}


function removePlayer(con) {
    for( var i = playerInfo.length; i--;){
        if(con.socket._peername.address + con.socket._peername.port ==
        playerInfo[i].socket._peername.address + playerInfo[i].socket._peername.port){
            playerInfo.splice(i, 1);
            return;
        }
    }
}


function SendEveryone(info){
    for(let p of playerInfo){
        sendInfo(p,info);
    }
}

function sendInfo(con, info) {
    con.sendUTF(info);
}

function originIsAllowed(origin) {
    let allowed = ["http://localhost","http://127.0.0.1","http://192.168.149.240"]
    return allowed.includes(origin);
}


var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    response.writeHead(404);
    response.end();
});


server.listen(3345, function() {
    console.log((new Date()) + ' Server is listening on port 3345');
});


wsServer = new WebSocketServer({
    httpServer: server,
    autoAcceptConnections: false
});

wsServer.on('request', function(request) {
    if (!originIsAllowed(request.origin)) {
      request.reject();
      console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
      return;
    }

    var connection = request.accept('game-protocol', request.origin);
    console.log((new Date()) + ' Connection accepted.');
    addPlayer(connection)
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            console.log(message.utf8Data)
            SendEveryone(message.utf8Data)
        }
    });

    connection.on('close', function(reasonCode, description) {
        removePlayer(connection);
        SendEveryone()
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
    });
});
